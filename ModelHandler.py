import falcon
from Custom_Helper import MakePredUtils
from multiprocessing import Pool
import imutils
import json
import uuid
import time
from cv2 import resize,imread
import os

class ModelHandler(object):

    def __init__(self):
        MakePredUtils.MultiGraphLoader()
        MakePredUtils.LoadDlib()
        self.proc_master = Pool()
        self.model_version = '1.0.0'
        self.GENDER_DICT = {1: 'M', 0: 'F'}

    def on_post(self, req, resp, **kwargs):
        try:
            start_time = time.time()
            img_stream = req.get_param('image_data')
            img_ext = req.get_param('image_ext')
            img_id = req.get_param('id')
            image = img_stream.file.read()
            img_name = '/app/images/'+str(img_id)+'_'+str(time.time())+'.'+img_ext
            with open(img_name,'wb') as f:
                f.write(image)
            image = imread(img_name)
            image_1_op = self.proc_master.apply_async(MakePredUtils.GetFace, args=(imutils.resize(image, width=512), False))
            image_2_op = self.proc_master.apply_async(MakePredUtils.GetFace, args=(imutils.resize(image, width=512), True))
            image_nimish = image_1_op.get()
            image_arsh = image_2_op.get()
            if image_nimish is not None or image_arsh is not None:
                p1 = self.proc_master.apply_async(MakePredUtils.PredictGender, args=(image_nimish,))
                p2_nimish = self.proc_master.apply_async(MakePredUtils.PredictAges, args=(resize(image_nimish,(299,299)),False,))
                p2_arsh = self.proc_master.apply_async(MakePredUtils.PredictAges, args=(resize(image_arsh,(128,128)),True,))
                p3 = self.proc_master.apply_async(MakePredUtils.PredictBmi, args=(img_name,))
                res1 = p1.get()
                res2_nimish = p2_nimish.get()
                res2_arsh = p2_arsh.get()
                res3 = p3.get()
                if res1 is not None and res2_nimish is not None and res2_arsh is not None and res3 is not None:
                    gender = self.GENDER_DICT[res1]
                    age =res2_nimish
                    print("Total Time: {}".format(time.time()-start_time))
                    resp.body = json.dumps({'status':200,'message':'successful','age':age,'gender':gender,'bmi':res3,'model_version':self.model_version})
                else:
                    resp.body = json.dumps({'status':500,"message":'System Failure'})
            else:
                resp.body = json.dumps({'status':400,'message':'Bad Selfie'})
            os.remove(img_name)
        except Exception as e:
            resp.body = json.dumps({'status':500,'message':str(e)})
            os.remove(img_name)

