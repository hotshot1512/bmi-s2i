import falcon
from ModelHandler import ModelHandler
from falcon_multipart.middleware import MultipartMiddleware
from Tester import TestProtocol

api = application = falcon.API(middleware=[MultipartMiddleware()])
api.add_route('/images',ModelHandler())
api.add_route('/hello',TestProtocol())
