import tensorflow as tf
import cv2
import numpy as np

with tf.gfile.GFile('model_age_arsh.pb', "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
with tf.Graph().as_default() as graph:
    tf.import_graph_def(
        graph_def,
        input_map=None,
        return_elements=None,
        name=""
        )
age_graph_nimish = graph

y_pred = age_graph_nimish.get_tensor_by_name("output_node0:0")
x = age_graph_nimish.get_tensor_by_name("input_1:0")
sess_n = tf.Session(graph=age_graph_nimish)

image = cv2.imread("test.jpeg")
image = cv2.resize(image,(100,100))
image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
image = np.array([image,image,image,image,image])
print(image.shape)
with sess_n:
    # ops = sess_n.graph.get_operations()
    # print(ops[0].values())    
    # print(ops[-1].values())
    res = sess_n.run(y_pred,feed_dict={x:image}).tolist()
    ages = []
    for k in res:
        print(k.index(max(k)))
        ages.append(k.index(max(k))+18)
    print(sum(ages)/len(ages))
    print(ages)
    # print(res[0].index(max(res[0])))