from fastai.basic_train import load_learner
from fastai.vision.image import open_image
from cv2 import COLOR_BGR2GRAY, COLOR_BGR2RGB, cvtColor
from imutils import face_utils
import face_recognition as fr
from pathlib import Path
import tensorflow as tf
import numpy as np
import joblib
import dlib

# Initialize global variables
age_graph_nimish = None
age_graph_arsh = None
gender_model = None
dlib_objects = None
bmi_graph = None
d_ages_nimish = np.arange(0, 101).reshape(101, 1)

#output_node0

def MultiGraphLoader():
    global age_graph_nimish
    global age_graph_arsh
    global gender_model
    global bmi_graph
    try:
        with tf.gfile.GFile('/app/Custom_Helper/saved_models/model_age_arsh.pb', "rb") as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
        with tf.Graph().as_default() as graph:
            tf.import_graph_def(
                graph_def,
                input_map=None,
                return_elements=None,
                name=""
                )
        age_graph_arsh = graph

        with tf.gfile.GFile('/app/Custom_Helper/saved_models/model_age_nimish.pb', "rb") as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
        with tf.Graph().as_default() as graph:
            tf.import_graph_def(
                graph_def,
                input_map=None,
                return_elements=None,
                name=""
                )
        age_graph_nimish = graph
        bmi_graph=load_learner('/app/Custom_Helper/saved_models/',fname='bmi_model.pkl')
        with open('/app/Custom_Helper/saved_models/gender_model.jlb', "rb") as f:
            gender_model = joblib.load(f)

        print("Loaded All Graphs...\n\n")
    except Exception as e:
        print("In Load Age/Gender/BMI")
        print(e)


def LoadDlib():
    global dlib_objects
    try:
        detector = dlib.get_frontal_face_detector()
        predictor = dlib.shape_predictor('/app/Custom_Helper/saved_models/shape_predictor_68_face_landmarks.dat')
        fa_1 = face_utils.FaceAligner(predictor, desiredFaceWidth=148)
        fa_2 = face_utils.FaceAligner(predictor, desiredFaceWidth=320)
        dlib_objects = (detector,predictor,fa_1,fa_2)
        print("Loaded Dlib")
    except Exception as e:
        print("In LoadDlib")
        print(e)


def GetFace(image, arsh_mode=False):
    global dlib_objects
    detector = dlib_objects[0]
    predictor = dlib_objects[1]
    if arsh_mode:
        fa = dlib_objects[2]
    else:
        fa = dlib_objects[3]
    gray_frame = cvtColor(image, COLOR_BGR2GRAY)
    rects = detector(gray_frame, 0)
    areas = list()
    face_id = 0
    if len(rects)>0:
        if len(rects)>1:
            for rect in rects:
                (x,y,w,h) = face_utils.rect_to_bb(rect)
                areas.append(w*h)
            face_id = areas.index(max(areas))
            (x,y,w,h) = face_utils.rect_to_bb(rects[face_id])
        else:
            for rect in rects:
                (x,y,w,h) = face_utils.rect_to_bb(rect)
        res = fa.align(image, gray_frame, rects[face_id])

        return cvtColor(res,COLOR_BGR2RGB)
    else:
        return None

def crop_center(img, cropx, cropy):
    y, x = img.shape[:2]
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)   
    return img[starty:starty+cropy,startx:startx+cropx]

def crop_edges(img, cropx, cropy):
    y, x = img.shape[:2]
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)
    return [img[:cropy, startx:startx+cropx], img[y-cropy:, startx:startx+cropx],
            img[starty:starty+cropy, :cropx], img[starty:starty+cropy, x-cropx:]]



def PredictAges(image,arsh_mode=False):
    global age_graph_nimish
    global age_graph_arsh
    global dlib_objects
    global d_ages_nimish


    detector = dlib_objects[0]
    img_h, img_w=299,299
    if image is not None:
        try:
            if arsh_mode:
                y_pred = age_graph_arsh.get_tensor_by_name("output_node0:0")
                x = age_graph_arsh.get_tensor_by_name("input_1:0")
                sess = tf.Session(graph=age_graph_arsh)
            else:
                y_pred = age_graph_nimish.get_tensor_by_name("output_node0:0")
                x = age_graph_nimish.get_tensor_by_name("input_1:0")
                sess_n = tf.Session(graph=age_graph_nimish)
            with sess:
                if arsh_mode:
                    crop_ims = [crop_center(image, 100, 100)]
                    crop_ims += crop_edges(image, 100, 100)
                    image = np.array(crop_ims)
                    res = sess.run(y_pred, feed_dict={x:image}).tolist()
                    ages = []
                    for k in res:
                        ages.append(k.index(max(k))+18)
                    final_age = sum(ages)/len(ages)
                    print("Arsh Output: ",final_age)
                else:
                    detected = detector(image, 1)
                    faces = np.empty((len(detected), 299, 299, 3))
                    for i, d in enumerate(detected):
                        x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
                        xw1 = max(int(x1 - 0.4 * w), 0)
                        yw1 = max(int(y1 - 0.4 * h), 0)
                        xw2 = min(int(x2 + 0.4 * w), img_w - 1)
                        yw2 = min(int(y2 + 0.4 * h), img_h - 1)
                        faces[i, :, :, :] = cv2.resize(img[yw1:yw2 + 1, xw1:xw2 + 1, :], (299,299))
                    results = sess.run(y_pred,feed_dict={x:faces})
                    predicted_ages = results.dot(d_ages_nimish).flatten()
                    final_age = 0
                    if len(predicted_ages)>1:
                        for g in predicted_ages:
                            if final_age<int(round(g,0)):
                                final_age = g
                    else:
                        final_age = int(round(predicted_ages[0],0))
                    print("Nimish Output: ",final_age)
            return final_age
        except Exception as e:
            print("In PredictAges")
            print(e)
    else:
        return None




def PredictGender(image):
    global gender_model
    if image is not None:
        try:
            result = gender_model.predict(fr.face_encodings(image))
            return result[0].tolist()
        except Exception as e:
            print("In PredictGender")
            print(e)
    else:
        return None


def PredictBmi(image_path):
    global bmi_graph
    if image_path is not None:
        try:
            Image_Path = Path(image_path)
            res = bmi_graph.predict(open_image(Image_Path))
            return str(res[0])
        except Exception as e:
            print("In PredictBMI")
            print(e)
    else:
        return None
