import falcon
import json

class TestProtocol(object):

	def __init__(self):
		print("Connection testing protocol started")


	def on_post(self,req, resp, **kwargs):
		try:
			resp.body = json.dumps({'status':200,'message':'connection successful'})
		except Exception as e:
			resp.body = json.dumps({'status':500,'message':str(e)})


	def on_get(self,req, resp):
		try:
			resp.body = json.dumps({'status':200,'message':'connection successful'})
		except Exception as e:
			resp.body = json.dumps({'status':500,'message':str(e)})
